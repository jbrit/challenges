class Node:
    def __init__(self, value):
        self.value = value
        self.next = None

    @staticmethod
    def create_router(values):
        # values is an integer array
        router = None
        current = None
        # iterate through the array, creating a linked list
        for i in range(len(values)):
            if i == 0:
                router = current = Node(values[i])
            else:
                current.next = Node(values[i])
                current = current.next
        return router


def identify_router(router):
    max_connections = float("-inf")
    max_values = []
    prev = None
    count = dict()

    while router:
        value = router.value
        #  initialize the value to 0 if it's not in the dictionary
        if value not in count:
            count[value] = 0
        count[value] += 1

        # if there's a previous node, increment the count
        if prev:
            count[value] += 1

        # if there's a next node, increment the count
        if router.next:
            count[value] += 1

        # if the current count is greater than the max, update the max
        if count[value] > max_connections:
            max_values = [str(value)]
            max_connections = count[value]
        # if the current count is equal to the max, add the value to the max_values
        elif count[value] == max_connections:
            max_values.append(str(value))

        # update the previous node and the current node
        prev = router
        router = router.next

    return ", ".join(max_values)


# Time complexity is O(n) - we only iterate through each node of the linked list once
