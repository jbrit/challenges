def compress(string: str):
    output = ""
    i = 0
    while i < len(string):
        # every time we encounter a new character, it's count is 1
        count = 1
        # keep incrementing the index until we find a character that is not the same
        while i + 1 < len(string) and string[i] == string[i + 1]:
            count += 1
            i += 1
        # add the character and its count to the output if it is more than 1
        output += string[i] + (str(count) if count != 1 else "")
        i += 1

    return output


# Time complexity is O(n) - we only iterate through the string once
